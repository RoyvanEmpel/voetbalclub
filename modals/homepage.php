<?php

//Modals zijn voor custom query's zodat heel gemakkelijk gewoon een functie aangeroepen kan worden.

class Homepage {

  private $db;
  function __construct()
  {
    $this->db = new DB;
  }

  public function showAll() //Ophalen alle users
  {
    return $this->db->query('SELECT `user_id`, `name`, `email`
                              FROM users
                            ')->result();
  }

  public function deleteSelf() //Zelf verwijderen
  {
    $this->db->delete("users", ['user_id' => $_SESSION['login']['user']['user_id']]);
  }

}

?>