<?php

class Clubs
{

  private $db;
  function __construct()
  { 
    $this->db = new DB();
  }

  public function getAll()
  {
    $query = "SELECT * FROM `clubs`";

    return $this->db->query($query)->result();
  }

  public function getUserClub($user_id)
  {
    $query = "SELECT `club_id` FROM `clubuser` c where c.`user_id` = $user_id";

    return $this->db->query($query)->row()->club_id ?? "";
  }
   
}
