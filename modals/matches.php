<?php

  class Matches {

    public $db;

    function __construct()
    {
      $this->db = new DB();
    }

    public function getAll()
    {
      $query = "SELECT m.`score_1` AS 'score_1', m.`score_2` AS 'score_2', c1.`name` AS 'name_1', c2.`name` AS 'name_2'
                FROM matches m
                JOIN clubs c1 ON m.`club_id_1` = c1.`club_id`
                JOIN clubs c2 ON m.`club_id_2` = c2.`club_id`";

      return $this->db->query($query)->result();
    }

  }

?>