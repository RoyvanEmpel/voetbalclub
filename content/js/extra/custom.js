// Dit js bestand wordt op elke pagina ingeladen.
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
  $('[data-toggle="tooltip"]').mouseleave(function() {
    $('.tooltip, .popover.fade').remove();
  });
})

function redirect(value, parameters='') {
  if (parameters!='') {
    var html = "";
    html = "<form id='redirect' action='' method='post'>";
    if (value != '') {
      html += '<input type="hidden" name="redirect" value="' + value + '">';
    }
    Object.keys(parameters).forEach(key => {
      html += "<input type='hidden' name='" + key + "' value='" + parameters[key] + "'>";
    });
    html += "</form>";
  } else {
    html = '<form id="redirect" action="" method="post"><input type="hidden" name="redirect" value="' + value + '"></form>';
  }
  $('link').html(html);
  $('#redirect').submit();
}