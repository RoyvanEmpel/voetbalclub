

function viewPassword()
{
  $('#bekijken').toggleClass("mdi-eye mdi-eye-off");


  if ($('#bekijken').siblings('input').attr('type') == 'password') {
    $('#bekijken').siblings('input').attr('type', 'text');
  } else {
    $('#bekijken').siblings('input').attr('type', 'password');
  }


  $('.tooltip, .popover.fade').remove();
  if ($('#bekijken').attr('data-original-title') == 'Wachtwoord bekijken') {
    $('#bekijken').attr('data-original-title', "Wachtwoord verbergen").tooltip('show');
  } else {
    $('#bekijken').attr('data-original-title', "Wachtwoord bekijken").tooltip('show');
  }
  
}