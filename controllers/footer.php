<?php

class Footer{

  private static $extraJS;

  function __controller() {
    self::$extraJS = [];
  }

  public static function extra($fileLocation) //Functie voor extra JS inladen
  {
    if(is_array($fileLocation)) {
      foreach ($fileLocation as $file) {
        self::$extraJS[] = $file;
      }
    } else {
      self::$extraJS[] = $fileLocation;
    }
  }
  
  public static function unloadJS($unloadFile = null) //Alle momenteel ingelade js unloaden
  {
    if ($unloadFile) {
      foreach (self::$extraJS as $index => $file) {
        if ($file == $unloadFile){
          unset(self::$extraJS[$index]);
        }
      }
    } else {
      self::$extraJS = [];
    }
  }

  public static function load() //Inladen van de JS
  {
    echo '
      </body>
    ';
    
    self::extra('extra/custom.js');

    if (!empty(self::$extraJS)) {
      foreach (self::$extraJS as $file) {
        echo '<script name="' . basename($file, '.js') . '">' . file_get_contents('content/js/' . $file) . '</script>';
      }
    }

    echo '</html>';
  }

}