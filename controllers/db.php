<?php
/**
 * Database
 */

include_once('conn.php');

class DB
{
	private static $conn;
	private static $result;

	private function conn() //Connectie aanmaken
	{
		try {
			self::$conn = new PDO("mysql:host=".Conn::$host.";dbname=".Conn::$db, Conn::$user, Conn::$pass);
		} catch (\Throwable $th) {
			throw $th;
		}
	}

	public function getConn() //Connectie terug geven (Speciale gevallen)
	{
		return self::$conn;
	}

	public function row() //1 Rij terug geven als een object
	{
		$result = $this->row_array();
		if (!empty($result)) {
			return (object)$result;
		} else {
			return [];
		}
	}

	public function row_array() //1 Rij terug geven als een array
	{
		try {
			if (!empty(self::$result)) {
				return self::$result->fetch(PDO::FETCH_ASSOC);
			}
			return [];
		} catch (\Throwable $th) {
			throw $th;
		}
		
	}

	public function result() //Alle rijen in een object terug geven
	{
		try {
			$result = new stdClass();
			foreach ($this->result_array() as $key => $value) {
				$result->{$key} = (object)$value;
			}
			return $result;
		} catch (\Throwable $th) {
			throw $th;
		}
	}

	public function result_array() //Alle rijen in een array terug geven
	{
		try {
			return self::$result->fetchAll(PDO::FETCH_ASSOC);
		} catch (\Throwable $th) {
			throw $th;
		}
	}

	public function query($sql) //Query functie voor het ophalen van data
	{
		if (!isset(self::$conn)) {
			$this->conn();
		}

		try {
			$stmt = self::$conn->prepare($sql);
			$stmt->execute();

			self::$result = self::$conn->query($sql);
			return $this;
		} catch (\Throwable $th) {
			print_pre("Faulthy query: $sql");
			throw $th;
		}
	}
	
	public function delete($table, $wheres) //Delete functie voor het verwijderen van data
	{
		if (!isset(self::$conn)) {
			$this->conn();
		}

		$query = "DELETE FROM `$table`";

		$query .= " WHERE";
		$i=1;
		foreach($wheres as $index => $where) {
			if ($i>1) { $query .= " AND";}
			$query .= " `$index` = '$where'";
			$i++;
		}
		
		try {
			$stmt = self::$conn->prepare($query);
			$stmt->execute();
		} catch (\Throwable $th) {
			print_pre("Faulthy query: $query");
			throw $th;
		}
	}

	public function insert($table, $input) //Insert functie voor het invoegen van data
	{
		if (!isset(self::$conn)) {
			$this->conn();
		}

		$prefix = $values = [];
		foreach ($input as $key => $value) {
			$prefix[] = $key;
			$values[] = $value;
		}

		$query = "INSERT INTO `$table` (" . implode(", ", $prefix). ") VALUES ('" . implode("', '", $values). "')";

		try {
			$stmt = self::$conn->prepare($query);
			$stmt->execute();
		} catch (\Throwable $th) {
			print_pre("Faulthy query: $query");
			throw $th;
		}
		
	}

	public function update($table, $update, $where) //Update functie voor het updaten van data
	{
		if (!isset(self::$conn)) {
			$this->conn();
		}

		$query = "UPDATE `$table` SET ";

		$i=1;
		foreach ($update as $key => $value) {
			$query .= ($i == 1 ? '' : ',') . "`$key` = '$value'";
			$i++;
		}

		$query .= " WHERE";
		$i=1;
		foreach($where as $index => $where) {
			if ($i>1) { $query .= " AND";}
			$query .= " `$index` = '$where'";
			$i++;
		}
		
		try {
			$stmt = self::$conn->prepare($query);
			$stmt->execute();
		} catch (\Throwable $th) {
			print_pre("Faulthy query: $query");
			throw $th;
		}
		
	}
}
?>