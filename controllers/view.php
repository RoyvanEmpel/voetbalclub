<?php 

class View {

  public static $data;
  public static function load($file = '', $data=[]) //Inladen van een pagina + header en footer laden.
  {
    if (isset($data['data'])) {
      foreach ($data['data'] as $key => $byte) {
        $data[$key] = $byte;
      }
  
      unset($data['data']);
    }
    self::$data = $data;

    Header::load();

    try {
      if (!empty($file)) {
        if (is_array($file)) {
          foreach ($file as $f) {
            include_once('views/' . $f . '.php');
          }
        } else {
          include_once('views/' . $file . '.php');
        }
      }
    } catch (\Throwable $th) {
      throw $th;
    }

    Footer::load();
  }

  public static function getFile($file) 
  {
    return file_get_contents('views/' . $file . '.php');
  }

}