<?php

  //Session starten
  session_start();

  // ERRORS AANZETTEN ADV LOKAAL/LIVE
  if ($_SERVER['SERVER_ADDR'] == '127.0.0.1' || $_SERVER['SERVER_NAME'] == 'localhost') {
    ini_set('display_errors', 1); 
    ini_set('display_startup_errors', 1); 
    error_reporting(E_ALL);
  } else {
    ini_set('display_errors', 0); 
    ini_set('display_startup_errors', 0); 
    error_reporting(0);
  }

  //Constants definen om ze overal te kunnen gebruiken.
  define('SITE_TITLE', 'Voetbalclub FCA');
  define('FAVICON', 'data:image/png;base64,' . base64_encode(file_get_contents('content/images/icon.jpeg')));

  //Inladen van helpers / controllers 
  include_once('helpers/variables.php');
	include_once('controllers/db.php');
  include_once('controllers/auth.php');
  include_once('controllers/header.php');
  include_once('controllers/footer.php');
  include_once('controllers/view.php');