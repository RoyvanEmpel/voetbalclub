<?php

class Header {

  public static $extraCSS;

  function __construct()
  {
    self::$extraCSS;
  }

  public static function extra($fileLocation) //Functie voor extra css inladen
  {
    if(is_array($fileLocation)) {
      foreach ($fileLocation as $file) {
        self::$extraCSS[] = $file;
      }
    } else {
      self::$extraCSS[] = $fileLocation;
    }
  }

  public static function unloadCSS($unloadFile = null) //Alle momenteel ingelade css unloaden
  {
    if ($unloadFile) {
      foreach (self::$extraCSS as $index => $file) {
        if ($file == $unloadFile){
          unset(self::$extraCSS[$index]);
        }
      }
    } else {
      self::$extraCSS = [];
    }
  }

  private static function head() //head data terug geven
  {
    if (isset(View::$data['title'])) {
      return'
              <meta charset="UTF-8"/>
              <title>' . View::$data['title'] . ' - ' . SITE_TITLE. '</title>
              <link rel="shortcut icon" href="' . FAVICON . '" type="image/ico">
            ';
    }

    return '
            <meta charset="UTF-8"/>
            <title>'.SITE_TITLE. ' - ' .$_SESSION['page'].'</title>
            <link rel="shortcut icon" href="' . FAVICON . '" type="image/ico">
          ';

  }

  private static function styles() //Alle styles/JS inladen
  {
    return '
            <style name="materialdesignicons">' . file_get_contents('content/css/mdi/materialdesignicons.min.css') . '</style>
            <style name="bootstrap">' . file_get_contents('content/css/bootstrap/bootstrap.min.css') . '</style>
            <style name="bootstrap-grid">' . file_get_contents('content/css/bootstrap/bootstrap-grid.min.css') . '</style>
            <style name="bootstrap-reboot">' . file_get_contents('content/css/bootstrap/bootstrap-reboot.min.css') . '</style>
            <style name="jquery-ui.min">' . file_get_contents('content/css/jquery-ui/jquery-ui.min.css') . '</style>
            <style name="jquery-ui.structure">' . file_get_contents('content/css/jquery-ui/jquery-ui.structure.min.css') . '</style>
            <style name="jquery-ui.theme">' . file_get_contents('content/css/jquery-ui/jquery-ui.theme.min.css') . '</style>
            <script name="jquery">' . file_get_contents('content/js/jquery/jquery.js') . '</script>
            <script name="bootstrap-bundle">' . file_get_contents('content/js/bootstrap/bootstrap.bundle.min.js') . '</script>
            <script name="datatables">' . file_get_contents('content/js/datatables/datatables.min.js') . '</script>
            <script name="sweetalert2" src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
            <script src="https://kit.fontawesome.com/d68d772542.js" crossorigin="anonymous"></script>
          ';
  }

  public static function load() //Opbouwen van de header.
  {
    echo '<html lang="en"><head>';
    echo self::head();
    echo self::styles();

    self::extra('extra/custom.css');

    if (!empty(self::$extraCSS)) {
      foreach (self::$extraCSS as $file) {
        echo '<style name="' . basename($file, '.css') . '">' . file_get_contents('content/css/' . $file) . '</style>';
      }
    }
      
    echo '</head><body>';
  }

}

new Header();