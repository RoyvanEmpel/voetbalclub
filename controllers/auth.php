<?php

	class Auth {
		private static $db;

		function __construct() { //Arrays aanmaken en $db variable vullen
			if (!isset($_SESSION['login']['errors'])) {
				$_SESSION['login'] = array();
				$_SESSION['login']['errors'] = array();
			}
			self::$db = new DB;
		}

		public static function verify_login() //Functie voor het checken of de user ingelogd is.
		{
			$input = $_SESSION['login']['user'];

			$email = strtolower(trim($input['email']));
			$password = trim($input['pass']);

			if (empty($email) || empty($password)) {
				return false;
			} else {

				try {
					$user = self::$db->query('SELECT * FROM users WHERE email = "' . $email . '"')->row();
					if (!empty($user)) {
						if ($password === $user->password || (isset($_SESSION['passUpdate']) && $_SESSION['passUpdate'] == 1)) {
							if (isset($_SESSION['passUpdate'])) { unset($_SESSION['passUpdate']); }
							self::loginUser($user);
							return true;
						}
					}

					self::addError('Niet ingelogd.');
					return false;
				} catch (\Throwable $th) {
					throw $th;
				}
				
			}
		}

		public static function login() //Functie inlog te verifieren
		{
			$input = $_POST;

			$email = strtolower(trim($input['email']));
			$password = trim($input['password']);

			if (empty($email) || empty($password)) {
				self::addSuccess('Lege velden zijn niet toegestaan.');
			} else {

				try {
					$user = self::$db->query('SELECT * FROM users WHERE email = "' . $email . '"')->row();
					if (!empty($user)) {
						if (self::hash($password) === strtoupper($user->password)) {
							self::loginUser($user);
						} else {
							self::addError('Het ingevoerde email of wachtwoord is onjuist.');
						}
					} else {
						self::addError('Het ingevoerde email of wachtwoord is onjuist.');
					}
				} catch (\Throwable $th) {
					//throw $th;
				}

			}
		}

		public static function register() //functie voor registreren
		{
			$input = $_POST;
			if (isset($input['email']) && !empty($input['email']) && 
					isset($input['name']) && !empty($input['name']) && 
					isset($input['password']) && !empty($input['password'])) {
	
				try {
					$email = strtolower(trim($input['email']));
					$name = trim($input['name']);
					$password = self::hash(trim($input['password']));
		
					$emailCheck = self::$db->query('SELECT * FROM users WHERE email = "' . $email . '"')->row();
					$role_id = self::$db->query('SELECT `role_id` FROM `roles` WHERE `role` = "Member"')->row()->role_id;
					if (empty($emailCheck)) {
						self::$db->insert('users', [
																				'role_id' => $role_id,
																				'email' => $email, 
																				'name' => $name, 
																				'password' => $password
																			]);

						self::addSuccess('Account successvol aangemaakt!');
						$_SESSION['page'] = 'login';
					} else {
						self::addError('Email wordt al gebruikt.');
					}
				} catch (\Throwable $th) {
					//throw $th;
				}
			} else {
				self::addError('Niet alle velden zijn correct ingevuld.');
				
			}
		}

		public static function loginUser($user) //Login de user
		{
			if (!isset($_SESSION['login']['user']) || empty($_SESSION['login']['user'])) {
				$_SESSION['login']['user']=[];
				$_SESSION['page'] = 'index';
			}
			$_SESSION['login']['user']['loggedIn'] = true;
			$_SESSION['login']['user']['user_id'] = $user->user_id;
			$_SESSION['login']['user']['name'] = $user->name;
			$_SESSION['login']['user']['email'] = $user->email;
			$_SESSION['login']['user']['pass'] = $user->password;
			$role = self::$db->query('SELECT `role` FROM `roles` WHERE `role_id` = "' . $user->role_id . '"')->row()->role;

			$_SESSION['login']['user']['role'] = $role;
			header("Refresh:0");
		}

		private static function addError($error) { //Error toevoegen om te laten zien
			$_SESSION['login']['errors'][] = $error;
		}

		private static function addSuccess($message) { //Success toevoegen om te laten zien
			$_SESSION['login']['success'][] = $message;
		}

		public static function hash($pass) //Functie voor sha512 Hashen en strtoupper
		{
			return strtoupper(hash('sha512', $pass));
		}

	}

	new Auth();