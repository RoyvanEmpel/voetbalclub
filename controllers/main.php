

<?php

  class Main {

    private $db;
    function __construct()
    {
      $this->db = new DB();

      if (!isset($_SESSION['page']) || empty($_SESSION['page'])) {
        $_SESSION['page'] = "index";
      }

      if (isset($_POST['redirect']) && !empty($_POST['redirect']) && method_exists($this, $_POST['redirect'])) {
        $_SESSION['page'] = $_POST['redirect'];
      } elseif (isset($_POST['redirect']) && !function_exists($_POST['redirect'])) {
        if (!isset($_SESSION['page']) || !method_exists($this, $_SESSION['page'])) {
          $_SESSION['page'] = "index";
          header("Refresh:0");
        } else {
          echo "<script>alert('Pagina bestaat niet.');</script>";
        }
      }

      $this->redirect();
    }

    private function redirect()
    {
      $adminPages = ['users', 'clubs'];
      $userPages = ['account'];
      if (in_array($_SESSION['page'], $adminPages)) {
        if (!isset($_SESSION['login']['user']['role']) || $_SESSION['login']['user']['role'] != "Administrator") {
          $_SESSION['page'] == "index";
          $this->logout();
        }
      }
      if (in_array($_SESSION['page'], $userPages)) {
        if (!isset($_SESSION['login']['user']['role']) || $_SESSION['login']['user']['role'] != "Member") {
          $_SESSION['page'] == "index";
          $this->logout();
        }
      }

      $this->{$_SESSION['page']}();
    }

    private function index()
    {
      Header::extra(['extra/partials/header.css', 'extra/index.css', 'extra/partials/footer.css']);
      Footer::extra('extra/index.js');
      View::load(['partials/header', 'index' ,'partials/footer'], ['title' => 'Hoofdpagina']);
    }

    private function login()
    {
      $this->db = new DB;
      if (!isset($_SESSION['login']['user']['loggedIn']) || $_SESSION['login']['user']['loggedIn'] === false) {
        if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['password'])) {
          Auth::register();
        } elseif (isset($_POST['email']) && isset($_POST['password'])) {
          Auth::login();
        }
      } else {
        if (!Auth::verify_login()) {
          $this->logout();
        }
      }

      Header::extra('extra/login.css');
      Footer::extra('extra/login.js');
      View::load('login', ['title' => 'Inloggen/Registreren']);
    }

    private function matches()
    {
      $data = new stdClass();
      
      include_once('modals/matches.php');

      $this->matches = new Matches();
      $data->matches = $this->matches->getAll();

      Header::extra(['extra/partials/header.css', 'extra/matches.css', 'extra/partials/footer.css']);
      // Footer::extra('extra/index.js');
      View::load(['partials/header', 'matches' ,'partials/footer'], ['title' => 'Matches', 'data' => $data]);
    }
    
    private function users()
    {
      $data = new stdClass();
      include_once('modals/users.php');
      $this->users = new Users();
      $data->users = $this->users->getAll();

      Header::extra(['extra/partials/header.css', 'extra/users.css', 'extra/partials/footer.css']);
      // Footer::extra('extra/users.js');
      View::load(['partials/header', 'users' ,'partials/footer'], ['title' => 'Gebruikers', 'data' => $data]);
    }

    private function account()
    {
      $data = new stdClass();
      include_once('modals/clubs.php');
      include_once('modals/users.php');
      $this->clubs = new Clubs();
      $this->users = new Users();

      if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['club']) && isset($_POST['password'])) {
        $user = $this->users->get($_SESSION['login']['user']['user_id']);
        $user->club_id = $this->clubs->getUserClub($user->user_id);
        

        if (!empty(trim($_POST['name'])) && trim($_POST['name']) != $_SESSION['login']['user']['name']) {
          $this->db->update('users', ['name' => trim($_POST['name'])], ['user_id' => $user->user_id]);
        }

        if (!empty(trim($_POST['email'])) && trim($_POST['email']) != $_SESSION['login']['user']['email']) {
          $this->db->update('users', ['email' => trim($_POST['email'])], ['user_id' => $user->user_id]);
        }

        if (!empty(trim($_POST['password'])) && Auth::hash(trim($_POST['password'])) != $_SESSION['login']['user']['pass']) {
          $this->db->update('users', ['password' => Auth::hash(trim($_POST['password']))], ['user_id' => $user->user_id]);
        }

        if (!empty(trim($_POST['club'])) && trim($_POST['club']) != $user->club_id) {
          $this->db->update('clubuser', ['club_id' => trim($_POST['club'])], ['user_id' => $user->user_id]);
          $user->club_id = $this->clubs->getUserClub($user->user_id);
        }

        $user = $this->users->get($_SESSION['login']['user']['user_id']);
        Auth::loginUser($user);
      }
      
      $data->clubs = $this->clubs->getAll();
      $data->user_club = $this->clubs->getUserClub($_SESSION['login']['user']['user_id']);

      Header::extra(['extra/partials/header.css', 'extra/account.css', 'extra/partials/footer.css']);
      // Footer::extra('extra/users.js');
      View::load(['partials/header', 'account' ,'partials/footer'], ['title' => 'Account', 'data' => $data]);
    }

    private function contact()
    {
      Header::extra(['extra/partials/header.css', 'extra/contact.css', 'extra/partials/footer.css']);
      View::load(['partials/header', 'contact' ,'partials/footer'], ['title' => 'Contact']);
    }

    private function clubs()
    {
      Header::extra(['extra/partials/header.css', 'extra/clubs.css', 'extra/partials/footer.css']);
      View::load(['partials/header', 'clubs' ,'partials/footer'], ['title' => 'Clubs']);
    }

    private function logout()
    {
      session_destroy();
      header("Refresh:0");
    }

  }

  new Main();