<?php

//print_pre Functie voor het makkelijk printen van arrays en objecten
if (!function_exists('print_pre')) {
 /**
  * @param mixed $var
  * @param boolean $die
  * 
  * @return void
  */
  function print_pre($var, $die = false)
  {

    echo '<pre>';
    print_r($var);
    echo '</pre>';

    if ($die) {
      die();
    }

  }
}
