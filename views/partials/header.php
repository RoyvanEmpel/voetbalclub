<header>
  <img src="content/images/voetbalveld.jpg" alt="" width="100%" height="90px">

  <nav class="navbar">
    <span  onclick="redirect('index')">Home</span>

    <?php if (isset($_SESSION['login']['user']['loggedIn']) && $_SESSION['login']['user']['loggedIn'] === true && $_SESSION['login']['user']['role'] == "Member") { ?> 
      <span  onclick="redirect('account')">Account</span> 
    <?php } ?>
    <?php if (isset($_SESSION['login']['user']['loggedIn']) && $_SESSION['login']['user']['loggedIn'] === true && $_SESSION['login']['user']['role'] == "Administrator") { ?> 
      <span  onclick="redirect('users')">Users</span>
      <span  onclick="redirect('clubs')">Clubs</span>
    <?php } ?>
    
    <span  onclick="redirect('matches')">Matches</span>
    <span  onclick="redirect('contact')">Contact</span>
  </nav>

  <div class="login">
    <?php if (!isset($_SESSION['login']['user']['loggedIn']) || $_SESSION['login']['user']['loggedIn'] === false) { ?>
      <span onclick="redirect('login')" class="btn btn-info">Inloggen / Registreren</span>
    <?php } else { ?>
      <span onclick="redirect('logout')" class="btn btn-info">Uitloggen</span>
    <?php } ?>
  </div>

</header>