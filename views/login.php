<div class="background"></div>
<div class="content" style="padding-bottom: 1%;">
  <div id="inlog-logo">
    <h1><?=SITE_TITLE; ?></h1>
  </div>
  <?php 
    if (isset($_SESSION['login']['errors'])) {
      foreach ($_SESSION['login']['errors'] as $message) { ?> 
        <div class="alert alert-danger" style="margin:15px;" role="alert">
          <?=$message; ?>
        </div>
      <?php } ?>
      <?php $_SESSION['login']['errors'] = []; ?>
    <?php } ?>
  <?php 
  if (isset($_SESSION['login']['success'])) {
    foreach ($_SESSION['login']['success'] as $message) { ?> 
      <div class="alert alert-success" style="margin:15px;" role="alert">
        <?=$message; ?>
      </div>
    <?php } ?>
    <?php $_SESSION['login']['success'] = []; ?>
  <?php } ?>
  <form method="post" class="m-t-10 p-b-10" action="">
    <?php if (isset($_SESSION['page']) && $_SESSION['page'] == 'register') { ?>
      <div class="mat-in">
        <input class='inputs required' type="text" name="name" value="<?=$_POST['name'] ?? ''; ?>" autocomplete="off" autofocus required>
        <span class="bar"></span>
        <label>Naam</label>
      </div> 
    <?php } ?>
    <div class="mat-in">
      <input class='inputs required' type="email" name="email" value="<?=$_POST['email'] ?? ''; ?>" autocomplete="off" required>
      <span class="bar"></span>
      <label>E-mail</label>
    </div>
    <div class="mat-in" style="display:flex;">
      <input class='inputs required' type="password" name="password" value="" autocomplete="off" required>
      <span class="bar"></span>
      <label>Wachtwoord</label>

      <i class="mdi mdi-eye" id="bekijken" onclick="viewPassword();" data-toggle="tooltip" data-placement="top" title="Wachtwoord bekijken"></i>
    </div>
    <?php if (isset($_SESSION['page']) && $_SESSION['page'] == 'register') { ?>
      <button type="submit" id="login">Register</button>
    <?php } else { ?>
      <button type="submit" id="login">Login</button>
    <?php } ?>

    <div class="text-right m-t-10">
      <?php if (isset($_SESSION['page']) && $_SESSION['page'] == 'register') { ?>
        <span class="link" onclick="redirect('login');">Terug naar inloggen.</span>
      <?php } else { ?>
        <span onclick="redirect('register');">Geen account? <span class="link">Registreer hier.</span></span>
      <?php } ?>
      <br><span class="link" onclick="redirect('logout');">Terug naar de hoofdpagina</span>

    </div>

  </form>
</div>