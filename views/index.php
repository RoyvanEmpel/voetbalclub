<content>
<h1>Welkom bij Voerbalclub FC Aalsmeer!</h1>
<h5>Hier onder staan onze gedragsregels.</h5>
<img src="https://www.fcaalsmeer.nl/wp-content/uploads/2016/12/logo_fcaalsmeer_white.png" id="FCA-logo">

<h1 class="m-t-50">Gedragsregels</h1>
<h2>1 Inleiding</h2>
Het bestuur van FC Aalsmeer wil er aan bijdragen dat de leden van de vereniging met plezier kunnen voetballen en zich als mens en sporter verder kunnen ontwikkelen. Normen en waarden bij de beoefening van de voetbalsport vinden we belangrijk. FC Aalsmeer wil actief werken aan de bewustwording bij spelers, leiders, trainers, en ouders/verzorgers op dit vlak. FC Aalsmeer moet een ontmoetingsplaats zijn waar sporters gezamenlijk met plezier en voldoening kunnen voetballen. Dit vraagt om duidelijke gedragsregels. Het bestuur heeft gedragsregels opgesteld die hierbij kunnen helpen. Zij dragen de gedragscode zelf actief uit. Vooral de trainers, leiders en ouders/verzorgers hebben een belangrijke verantwoordelijkheid en voorbeeldfunctie voor wat betreft het uitdragen en bewaken van gedragsregels. Bij overtreding van de regels kunnen sancties volgen.

<h2>2 Algemeen</h2>
De gedragscode geldt voor alle leden. Voor iedereen geldend zijn de algemene gedragsregels opgesteld (dus voor leden en bezoekers) en per doelgroep (spelers, trainers, leiders, ouders/verzorgers) zijn specifieke gedragsregels opgesteld.
   2.1 Doelstelling van de vereniging
Het bestuur van FC Aalsmeer wil alle leden binnen hun persoonlijke mogelijkheden zo goed mogelijk laten voetballen.
Enthousiasme, ontspanning en plezier zijn daarbij de belangrijkste elementen. Het streven is om alle teams op een dusdanig niveau aan de competitie te laten deelnemen dat voldoende uitdaging biedt om beter te gaan voetballen. Selectieteams spelen op het hoogst haalbare niveau. Door ontwikkeling van zowel het voetballend vermogen als het verenigingsgevoel willen we bewerkstelligen dat FC Aalsmeer een vereniging is om trots op te zijn.
   2.2 Voor wie gelden de regels?
Iedereen die lid is, lid wil worden, dan wel als trainer/leider/vrijwilliger bij FC Aalsmeer actief zijn, moet van de gedragscode op de hoogte zijn en geeft aan zich hieraan te houden door middel van zijn/haar handtekening. Voor jeugdleden geldt bovendien dat ook de ouders/verzorgers op de hoogte moeten zijn van de gedragscode. De gedragscode moet door iedereen bij FC Aalsmeer worden uitgedragen en nageleefd. We moeten elkaar hierop kunnen en durven aanspreken. Goede omgangsvormen vormen het uitgangspunt voor ons handelen.
   2.3 Waarover gaat het?
Het gaat over hoe we met elkaar om willen gaan. En over regels. En hoe we de deze regels handhaven. Het gaat over wat we als club en met elkaar normaal vinden en niet normaal vinden. We noemen dit: Normen en Waarden. We spreken met iedereen af wat we normaal vinden en wat niet. Op basis daarvan stellen we duidelijke regels. Wat gebeurt er als mensen (spelers, trainers, leiders, ouders) zich niet aan de regels van de gedragscode houden? We nemen dan maatregelen, we noemen dit de sancties.

<h2>3. Algemene gedragsregels</h2>
Het bestuur van FC Aalsmeer vraagt nadrukkelijk aandacht voor Normen en Waarden voor, tijdens en na beoefening van de voetbalwedstrijden en trainingen. Hieronder volgt een aantal algemene regels die zowel op het voetballen als op het gebruik en het betreden van het voetbalcomplex van toepassing zijn.
   3.1 Wat zijn onze uitgangspunten?
Voetbal is een teamsport, dat je met elkaar en met de tegenstander gezamenlijk beoefent. Zonder samenspel geen voetbal en zonder tegenstander geen wedstrijd.
• Wij gedragen ons sportief, ook als anderen minder sportief zijn.
• Wij hebben altijd respect voor de scheidsrechter, ook als deze een fout maakt.
• Wij hebben respect voor de tegenstanders, de leiders, trainers en het publiek.
• Wij vernielen niet andermans eigendom en wij nemen niet andermans eigendom ongevraagd mee.
• Wij discrimineren, pesten/treiteren, irriteren en kwetsen elkaar niet.
• Wij houden ons aan onze afspraken.
• Wij spreken elkaar op fout gedrag aan.
Discriminatie, schelden, grof taalgebruik, treiteren, pesten, irriteren of kwetsen van wie dan ook wordt niet geaccepteerd en kunnen aanleiding zijn voor sancties.
Deze uitgangspunten gelden voor iedereen, dus spelers, ouders, leiders, trainers, vrijwilligers, niemand uitgezonderd!
   3.2 Op en rond het sportcomplex
Het sportcomplex is van ons allemaal. Wees er zuinig op en zorg, dat het netjes blijft. Denk daarbij in ieder geval aan de volgende punten:
• Maken we rommel, dan ruimen we dat zelf op.
• Er wordt geen glas- en aardewerk buiten de kantine meegenomen.
• Blijf achter de hekken/reclameborden tijdens een wedstrijd.
• Voetbalschoenen zijn niet toegestaan in de kantine.
• Houden we ons aan de voorschriften zoals vermeld op de borden aan de kunstgrasvelden.
• Halen wij de alle pupillen- en trainingsdoelen van het veld na de laatste training of wedstrijd.
• Gebruik de materialen en meubels waarvoor ze bedoelt zijn.
• Fietsen en brommers worden in de daarvoor bestemde rekken geplaatst.
• Auto’s worden geparkeerd op het daarvoor bedoelde parkeerterrein.
• Laat vanwege veiligheid geen kinderen rennen of voetballen op het parkeerterrein.
• De toegangen naar de velden in het belang van de veiligheid vrijhouden.
• Degene die zich hier niet aan houdt wordt op dat gedrag aangesproken.
   3.3 Lidmaatschap en contributie
Voor dat men lid wordt van onze club kunnen nieuwe leden eerst mee trainen. Zodra ze deel gaan nemen aan wedstrijden zullen zij lid moeten zijn van FC Aalsmeer. Het nieuwe lid dient zelf zorg te dragen dat het lidmaatschapsformulier bij de ledenadministratie terecht komt.
De contributie dient voor aanvang van het seizoen zijn voldaan. Spelers die hier niet aan voldoen, kunnen niet meedoen aan de wedstrijden.
Als je je lidmaatschap wilt opzeggen, is dit definitief omdat voorlopige of onder voorbehoud opzeggingen niet meer mogelijk zijn, gelden de volgende regels:

Geef via een email (naar ledenadministratie) aan dat het lidmaatschap wordt opgezegd. Dit dient elk seizoen vóór 31 mei te gebeuren:
Mondelinge opzeggingen of opzeggingen via WhatsApp of e-mail aan bijvoorbeeld de trainer/ leider/een bestuurslid, worden niet geaccepteerd;
Als er geen opzegging vóór 31 mei is ontvangen, zijn er kosten aan de opzegging verbonden;
Wordt er nog opgezegd voor de uiterste overschrijvingsdatum (15 juni 2020), zijn de kosten € 25;
Wordt er na de overschrijvingsperiode opgezegd, maar voor aanvang van de nieuwe competitie zijn de kosten €50;
Indien er niet opgezegd is volgens bovenstaande regels, dan blijft de speler lid van FC Aalsmeer en dient de complete contributie voor het nieuwe seizoen betaald te worden.
Voordat wij akkoord kunnen gaan met opzegging dienen uiteraard uitstaande boetes, kleding bruikleen en/of (gedeeltelijke) contributies van het/de afgelopen seizoen(en) betaald te zijn en/of openstaande vorderingen van ons voldaan te zijn.
Als je (een lid) wilt overschrijven naar een andere vereniging, gelden de dezelfde regels als bij opzegging (zie hierboven) met uitzondering van de uiterste datum. Overschrijvingen dienen vóór 16 mei te worden aangevraagd. Daarnaast geldt, als vóór 16 mei 2020 niet is opgezegd, het volgende:

Als overschrijving wordt aangevraagd vóór de uiterste overschrijvingsdatum (15 juni 2020), zijn de kosten €25. Na deze datum kan er geen overschrijving meer plaatsvinden in de A-categorie (dit zijn de hogere selectieteams);
Na de uiterste overschrijvingsdatum is het nog wel mogelijk om overschrijving aan te vragen voor de B-categorie (alle andere teams, niet zijnde de hogere selectieteams). Als dit gebeurt voor aanvang van de nieuwe competitie zijn de kosten €50;
Indien de overschrijving pas bij aanvang van het nieuwe seizoen ingediend wordt, zal de volledige contributie betaald moeten worden.
<h2>4 Waarden, normen en regels</h2>
   4.1 Voetballers
Waarden:
• Probeer te winnen met respect voor jezelf, je teamgenoten en je tegenstanders
• Vindt eerlijk en prettig spelen belangrijk en presteert zo goed mogelijk
• Onsportief gedrag van de tegenstander is nooit een reden om zelf onsportief te zijn
• Je accepteert en respecteert de beslissingen van de scheidsrechter

Normen:
• Wijs je medespelers op onsportief of onplezierig gedrag
• Je gaat zorgvuldig met het wedstrijd- en trainingsmateriaal om
• Respecteer het werk van al die mensen die ervoor zorgen dat je kan voetballen/trainen
• Je accepteert de aanwijzingen van de trainer

Regels:
• Geef de tegenstander een hand na de wedstrijd en wens de tegenstander geluk of feliciteer hem/haar met de winst.
• Kom op tijd op de trainingen en wedstrijden. Bij te laat komen zal de trainer/leider een passende sanctie opleggen.
• Wanneer je verhinderd bent voor een wedstrijd of training geef je dit tijdig aan bij je trainer/leider.
• Alle selectiespelers komen in hun trainingspak naar de voetbal, dit om de sponsor te respecteren en voor een professionele uitstraling. Na de wedstrijd ben je vrij om andere kledij te dragen.
• Voetbalschoenen worden buiten schoongemaakt op de daarvoor bestemde borstels. Niet binnen tegen de muren van de kleedkamers.
• Bij FC Aalsmeer is het verplicht om te douchen na de wedstrijd, vanaf de O12 wordt er bij FC Aalsmeer gedoucht na de training.
• Kleedkamers dienen schoon en aangeveegd achtergelaten te worden.
• Het dragen van sieraden tijdens een wedstrijd en training zijn niet toegestaan.
• Geen vernielingen aanrichten. Eventueel aangebrachte schade zal op de veroorzaker worden verhaald.
• Van andermans eigendommen afblijven. Bij gerede verdenking op diefstal zal aangifte worden gedaan bij de politie.
• Mobiele telefoons moeten geweerd worden uit de kleedkamers, dit in verband met het uitlokken tot diefstal, maar ook om privacy redenen van de medespelers.
• Bij (herhaald) wangedrag zal men als lid worden geroyeerd.
• Voor meer specifieke informatie Kleinveld, zie het document Help ik ben leider en nu. Deze is op te vragen bij de betreffende coördinator.

   4.2 Ouders
Waarden:
• Kinderen sporten voor hun eigen plezier en niet de uwe.
• Forceer een kind nooit als het geen interesse toont om deel te nemen aan een sport.
• Moedig uw kind aan om volgens de regels te spelen.
• Leer de kinderen positief gedrag, acceptatie.
• Maak ze duidelijk dat sport meer is als alleen maar winnen of verliezen.
• Erken de waarde en het belang van alle vrijwillige kaderleden rondom uw kind. Zij geven hun tijd en kennis om het sporten van uw kind mogelijk te maken.

Normen:
• Moedig de spelers aan.
• Assisteer waar nodig de trainer/begeleider, bijvoorbeeld door doeltjes te verplaatsen of in te vallen als grensrechter.
• Zorg dat uw kind op tijd is bij de wedstrijden en op de trainingen.
• Toon betrokkenheid bij uw kind door, zo vaak als mogelijk, te kijken bij een wedstrijd.
• Bedank eens de trainer/leider en andere vrijwilligers die wedstrijden of andere activiteiten leiden.

Regels:
• Geef uw kind of andere kinderen geen aanwijzingen tijdens de training of wedstrijd. Kinderen waarderen dit vaak niet, maar het is ook erg verwarrend als ze van meerdere kanten verschillende informatie krijgen.
• Val een beslissing van een scheidsrechter niet in het openbaar af en trek nooit de integriteit van dergelijke personen in twijfel.
• Val ook een beslissing van een trainer/leider met betrekking tot wissels, tactiek en andere voetbal gerelateerde zaken niet in het openbaar af. Bespreek dit soort zaken na een wedstrijd met de betreffende trainer/leider of maak een aparte afspraak met hen.
• Blijf tijdens de wedstrijd ruim buiten de lijnen.
• Bij het ongewenst betreden van het veld of andere misdragingen zult u van het complex worden verwijderd.
• Specifiek voor kinderen in de F en E. De kinderen worden afgeleverd bij het veld en dus niet op de parkeerplaats. Op deze manier kunnen de trainers te allen tijde communiceren met de ouders. Ook bij het ophalen worden de kinderen niet bij het hek opgehaald maar bij het veld.
• Zie het document Help ik ben leider en nu, voor meer specifieke informatie mbt het kleinveld.

   4.3 Trainers/leiders
Waarden:
• Bedenk dat kinderen voor hun plezier spelen en iets willen leren.
• Ontwikkel teamrespect voor teamspelers onderling, voor de tegenstander en voor de scheidsrechter.
• Geef de spelers een compliment wanneer dit verdiend is.
• De trainers/leiders hebben een voorbeeldfunctie, dus respecteer ouders, wedstrijdleiding en tegenstanders.

Normen:
• Leer spelers dat de spelregels gerespecteerd moeten worden.
• Corrigeer het gedrag van individuele spelers indien noodzakelijk.
• Schreeuw niet onnodig naar de spelers.
• Maak de kinderen nooit belachelijk als zij fouten maken of een wedstrijd verliezen.

Regels:
• Zorg ervoor, dat het trainingsmateriaal voldoet aan de veiligheidseisen.
• Zorg ervoor, dat er zuinig wordt omgegaan met het trainingsmateriaal en dat alles compleet blijft.
• Tijdens het uitoefenen van je functie gebruik je geen alcohol.
• Zorg voor een goede balans in speeltijd tussen de betere en minder goede spelers.
• Volg de aangeboden cursussen en opleidingen.
• Tijdens het uitoefenen van je functie, draag je te allen tijde je trainersjas. Dit om de sponsor te respecteren en om de professionele uitstraling van de club te waarborgen.
• Voor meer specifieke informatie Kleinveld, zie het document Help ik ben leider en nu. Deze is op te vragen bij de betreffende coördinator.

<h2>5. Alcohol, tabak en drugs</h2>
Het geven van het goede voorbeeld door volwassenen is van groot belang. Omdat het voorbeeld niet alleen volwassenen betreft, maar in heel veel gevallen juist jonge kinderen, dienen onderstaande zaken apart genoemd te worden:
• Alcohol en tabak zijn middelen die onze gezondheid schaden. Gebruik ze met mate en zeker niet op het veld of in de kleedkamers. Wees er van bewust dat het gebruik van alcohol en tabak in het bijzijn van jeugd een slecht voorbeeld is voor de kinderen.
• Het is ten strengste verboden te roken in de openbare ruimtes zoals kantine, toiletten en kleedkamers.
• Het nuttigen van alcoholische dranken in de bestuurskamer en de kantine is op wedstrijddagen voor 13:00 uur niet toe gestaan.
• Aanwijzingen van het barpersoneel dienen altijd te worden opgevolgd.
• Aan personen jonger dan 18 jaar worden geen alcoholhoudende dranken geschonken.
• Aan personen onder invloed wordt geen alcohol geschonken
• De club zorgt voor een gevarieerd aanbod aan niet-alcoholhoudende dranken en alcoholgebruik wordt niet gepromoot.
• Drugsbezit en drugsgebruik in en om het sportcomplex/parkeerplaats is niet toegestaan en zal direct leiden tot een veld/complex verbod.

<h2>6. Sancties</h2>
Overtredingen van de gedragsregels of ontoelaatbaar gedrag leiden tot sancties opgelegd door de vereniging. Het bestuur heeft een viertal sancties gedefinieerd, te weten:
• Sanctie 1: waarschuwing
• Sanctie 2: officiële waarschuwing
• Sanctie 3: schorsen van een lid voor bepaalde tijd
• Sanctie 4: royeren van een lid
In onderstaand overzicht worden voorbeelden gegeven van de overtreding van de gedragsregels of ontoelaatbaar gedrag met daaraan verbonden sancties. Bij overtredingen die niet beschreven zijn, zal het bestuur passende sancties vaststellen afhankelijk van de ernst van de overtreding.
</content>