<content>
  <div class="row">
    <div class="col-2"></div>
    <div class="col-4"><span class="btn btn-block btn-info">Input User</span></div>
    <div class="col-4"><span class="btn btn-block btn-info">Edit User</span></div>
    <div class="col-2"></div>
  </div>
  <div class="row m-t-10">
    <div class="col-2"></div>
    <div class="col-4"><span class="btn btn-block btn-info">Show Not Paid</span></div>
    <div class="col-4"><span class="btn btn-block btn-info">Delete User</span></div>
    <div class="col-2"></div>
  </div>
  <div class="row m-t-10">
    <div class="col-2"></div>
    <div class="col-4"><span class="btn btn-block btn-info" onclick="showAll()">Show All users</span></div>
    <div class="col-4"><span class="btn btn-block btn-info">Search User</span></div>
    <div class="col-2"></div>
  </div>
  <div class="row m-t-10">
    <div class="col-2"></div>
    <div class="col-4"><span class="btn btn-block btn-info">Print in PDF</span></div>
    <div class="col-4"></div>
    <div class="col-2"></div>
  </div>

  <br>

  <div id="content">
    <table>
      <thead>

      </thead>
      <tbody>
        
      </tbody>
    </table>
  </div>

</content>