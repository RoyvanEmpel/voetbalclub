<content>

  <div class="alert alert-info">
    <h1>Hallo <?=ucfirst($_SESSION['login']['user']['name']); ?>!</h1>
    <h5>Hier kan jij jouw persoonlijke gegevens aanpassen.</h5>
  </div>

  <form action="" method="post">
    <div class="row">
      <div class="col-1">Naam:</div>
      <div class="col-11">
        <input type="text" name="name" value="<?=$_SESSION['login']['user']['name']; ?>">
      </div>
    </div>
    <div class="row m-t-10">
      <div class="col-1">E-mail:</div>
      <div class="col-11">
        <input type="email" name="email" value="<?=$_SESSION['login']['user']['email']; ?>">
      </div>
    </div>
    <div class="row m-t-10">
      <div class="col-1">Wachtwoord:</div>
      <div class="col-11">
        <input type="password" name="password">
      </div>
    </div>
    <div class="row m-t-10">
      <div class="col-1">Club:</div>
      <div class="col-11">
        <select name="club">
          <?php foreach ($data['clubs'] as $club) { ?>
            <option value="<?=$club->club_id; ?>" <?=($data['user_club'] == $club->club_id ? 'selected': ''); ?>> <?=$club->name . " - " . ucfirst($club->city); ?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <button class="btn btn-info m-t-20">Opslaan</button>
  </form>
  
</content>