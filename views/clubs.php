<content>
  <div class="row">
    <div class="col-2"></div>
    <div class="col-4"><span class="btn btn-block btn-info">Input Club</span></div>
    <div class="col-4"><span class="btn btn-block btn-info">Edit Club</span></div>
    <div class="col-2"></div>
  </div>
  <div class="row m-t-10">
    <div class="col-2"></div>
    <div class="col-4"><span class="btn btn-block btn-info">Show All Clubs</span></div>
    <div class="col-4"><span class="btn btn-block btn-info">Delete Club</span></div>
    <div class="col-2"></div>
  </div>
  <div class="row m-t-10">
    <div class="col-2"></div>
    <div class="col-4"><span class="btn btn-block btn-info">Search Club</span></div>
    <div class="col-4"></div>
    <div class="col-2"></div>
  </div>
</content>