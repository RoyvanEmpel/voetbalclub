<content>
  <article class="post-listing post post-238 page type-page status-publish " id="the-post">
    <div class="google-map "><iframe loading="lazy" width="658" height="330" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.nl/maps?q=+Beethovenlaan+120&amp;hl=nl&amp;ll=52.255017,4.772412&amp;spn=0.005891,0.016512&amp;sll=52.212992,5.27937&amp;sspn=6.039478,16.907959&amp;hnear=Beethovenlaan+120,+1431+WZ+Aalsmeer,+Noord-Holland&amp;t=h&amp;z=17&amp;iwloc=A&amp;output=embed" data-rocket-lazyload="fitvidscompatible" data-lazy-src="https://www.google.nl/maps?q=+Beethovenlaan+120&amp;hl=nl&amp;ll=52.255017,4.772412&amp;spn=0.005891,0.016512&amp;sll=52.212992,5.27937&amp;sspn=6.039478,16.907959&amp;hnear=Beethovenlaan+120,+1431+WZ+Aalsmeer,+Noord-Holland&amp;t=h&amp;z=17&amp;iwloc=A&amp;output=embed" class="lazyloaded" data-was-processed="true"></iframe><noscript><iframe width="658" height="330" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.nl/maps?q=+Beethovenlaan+120&amp;hl=nl&amp;ll=52.255017,4.772412&amp;spn=0.005891,0.016512&amp;sll=52.212992,5.27937&amp;sspn=6.039478,16.907959&amp;hnear=Beethovenlaan+120,+1431+WZ+Aalsmeer,+Noord-Holland&amp;t=h&amp;z=17&amp;iwloc=A&amp;output=embed"></iframe></noscript></div>
    <div class="post-inner">
      <h1 class="name post-title entry-title">Adres &amp; route</h1>
      <p class="post-meta"></p>
      <div class="clear"></div>
      <div class="entry">
        <p>Het postadres van FC Aalsmeer is:<br> Postbus 1336<br> 1430 BH Aalsmeer</p>
        <p>Bezoekadres:<br> Sportpark “Hornmeer”<em> (voormalig RKAV)</em><br> Beethovenlaan 120<br> 1431 WZ Aalsmeer</p>
        <p><iframe loading="lazy" style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2442.405022837908!2d4.771900099999987!3d52.254189799999985!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c5de3d2b93448d%3A0xb968a5b2daf5e919!2sBeethovenlaan+120!5e0!3m2!1snl!2snl!4v1402426534650" width="400" height="300" frameborder="0" data-rocket-lazyload="fitvidscompatible" data-lazy-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2442.405022837908!2d4.771900099999987!3d52.254189799999985!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c5de3d2b93448d%3A0xb968a5b2daf5e919!2sBeethovenlaan+120!5e0!3m2!1snl!2snl!4v1402426534650" class="lazyloaded" data-was-processed="true"></iframe><noscript><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2442.405022837908!2d4.771900099999987!3d52.254189799999985!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c5de3d2b93448d%3A0xb968a5b2daf5e919!2sBeethovenlaan+120!5e0!3m2!1snl!2snl!4v1402426534650" width="400" height="300" frameborder="0"></iframe></noscript></p>
      </div>
      <div class="share-post"> <span class="share-text">Deel</span>
        <ul class="flat-social">
          <li><a href="http://www.facebook.com/sharer.php?u=https://www.fcaalsmeer.nl/adres-route/" class="social-facebook" rel="external" target="_blank"><i class="fa fa-facebook"></i> <span>Facebook</span></a></li>
          <li><a href="https://twitter.com/intent/tweet?text=Adres+%26+route via %40fcaalsmeer&amp;url=https://www.fcaalsmeer.nl/adres-route/" class="social-twitter" rel="external" target="_blank"><i class="fa fa-twitter"></i> <span>Twitter</span></a></li>
          <li><a href="https://plusone.google.com/_/+1/confirm?hl=en&amp;url=https://www.fcaalsmeer.nl/adres-route/&amp;name=Adres+%26+route" class="social-google-plus" rel="external" target="_blank"><i class="fa fa-google-plus"></i> <span>Google +</span></a></li>
          <li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=https://www.fcaalsmeer.nl/adres-route/&amp;title=Adres+%26+route" class="social-linkedin" rel="external" target="_blank"><i class="fa fa-linkedin"></i> <span>LinkedIn</span></a></li>
          <li><a href="http://pinterest.com/pin/create/button/?url=https://www.fcaalsmeer.nl/adres-route/&amp;description=Adres+%26+route&amp;media=" class="social-pinterest" rel="external" target="_blank"><i class="fa fa-pinterest"></i> <span>Pinterest</span></a></li>
        </ul>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  </article>
</content>