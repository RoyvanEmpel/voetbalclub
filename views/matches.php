<content>

  <?php if (!isset($_SESSION['login']['user']) || $_SESSION['login']['user']['role'] == "Member") { ?>

    <table class="table table-hover table-responsive table-striped">
      <thead>
        <tr>
          <th colspan="3" style="text-align: center;"><span style="font-size: x-large;">Overzicht</span></th>
        </tr>
      </thead>
      <tbody>

        <?php foreach ($data['matches'] as $key => $match) { ?>

          <tr class="match">
            <td><?=$match->name_1; ?></td>
            <td><?=$match->score_1; ?>&nbsp;:&nbsp;<?=$match->score_2; ?></td>
            <td><?=$match->name_2; ?></td>
          </tr>

        <?php } ?>

      </tbody>
    </table>

  <?php } elseif (!isset($_SESSION['login']['user']) || $_SESSION['login']['user']['role'] == "Administrator") { ?>

    <div class="row">
      <div class="col-2"></div>
      <div class="col-4"><span class="btn btn-block btn-info">Add Match</span></div>
      <div class="col-4"><span class="btn btn-block btn-info">Show All Matches And Results</span></div>
      <div class="col-2"></div>
    </div>
    <div class="row m-t-10">
      <div class="col-2"></div>
      <div class="col-4"><span class="btn btn-block btn-info">Add Result</span></div>
      <div class="col-4"><span class="btn btn-block btn-info">Delete Match</span></div>
      <div class="col-2"></div>
    </div>
    <div class="row m-t-10">
      <div class="col-2"></div>
      <div class="col-4"><span class="btn btn-block btn-info">Show All Matches</span></div>
      <div class="col-4"></div>
      <div class="col-2"></div>
    </div>

  <?php } ?>

</content>